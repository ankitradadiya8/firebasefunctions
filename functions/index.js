const functions = require('firebase-functions');

const admin = require('firebase-admin');
admin.initializeApp(functions.config().firebase);
const db = admin.firestore();

exports.rewardNotification = functions.firestore.document('notification/users').onWrite((change, context) => {
     const newValue = change.after.data();
     console.log("rewardNotification", newValue);
	 
	 if(newValue && newValue.text && newValue.token) {
		 const payload = {
			data: {
				title: newValue.title,
				body: newValue.text,
				sound: "default"
			},
		};

	    //Create an options object that contains the time to live for the notification and the priority
		const options = {
			priority: "high",
			timeToLive: 60 * 60 * 24
		};

		return admin.messaging().sendToDevice(newValue.token, payload, options);
	 }
	 else {
		 return false;
	 }
});

exports.referReward = functions.firestore.document('users/{userId}').onWrite((change, context) => {
     const newValue = change.after.data();
     console.log("referReward", newValue);
	 
	 const referredBy = newValue.referredBy;
	 const isReferDone = newValue.isReferDone;
	 const score = newValue.score;
	 var reward = 20;
	 //var batch = db.batch();

	 if (isReferDone !== null && isReferDone === false && referredBy !== null && score !== null && score >= 100) {
	 	
	 	var referredByRef = db.collection('users').doc(referredBy);
	 	var userRef = db.collection('users').doc(context.params.userId);

	 	var currentUserBalance = newValue.balance + reward;  // 20 rupees add to current user's balance

	 	return db.runTransaction(transaction => {
	        return transaction.get(referredByRef).then(referredDoc => {
	          console.log("All Done", referredDoc.data());

	          // Compute referred balance
	          var newBalance = referredDoc.data().balance + reward;  // 20 rupees add to referred user's balance
	          var title = "CrickBetting";
	          var body = "Congratulations! You've earned ₹" + reward + " for referral reward from your friend";

	          const registrationTokens = [newValue.token, referredDoc.data().token];
	          const message = {
				  data: {
					title: title,
					body: body,
					sound: "default"},
				  tokens: registrationTokens,
				}

	          // Update Balance info
	          transaction.update(referredByRef, { balance: newBalance }, { merge: true });
	          transaction.update(userRef, { balance: currentUserBalance, isReferDone : true }, { merge: true });

			  return admin.messaging().sendMulticast(message);
	        });
	    })
	    .then(function () {
		  console.log("Successfully Referred!");
		}).catch(function (err) {
		  console.error(err);
		});
	 } else {
	 	return false;
	 }
});

exports.cloudNotification = functions.firestore.document('notification/broadcast').onWrite((change, context) => {
     const newValue = change.after.data();
	 
	 if(newValue) {
		 const payload = {
			data: {
				title: newValue.title,
				body: newValue.text,
				sound: "default"
			},
		};

	    //Create an options object that contains the time to live for the notification and the priority
		const options = {
			priority: "high",
			timeToLive: 60 * 60 * 24
		};

		return admin.messaging().sendToTopic("crickbetting", payload, options);
	 }
	 else {
		 return false;
	 }
});

exports.cloudTestNotification = functions.firestore.document('notification/test').onWrite((change, context) => {
     const newValue = change.after.data();
	 
	 if(newValue) {
		 const payload = {
			data: {
				title: newValue.title,
				body: newValue.text,
				sound: "default"
			},
		};

	    //Create an options object that contains the time to live for the notification and the priority
		const options = {
			priority: "high",
			timeToLive: 60 * 60 * 24
		};

		return admin.messaging().sendToTopic("testing", payload, options);
	 }
	 else {
		 return false;
	 }
});

